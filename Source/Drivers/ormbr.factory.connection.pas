{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.factory.connection;

interface

uses
  Classes,
  DB,
  ormbr.factory.interfaces,
  ormbr.types.database,
  ormbr.monitor,
  ormbr.driver.connection;

type
  /// <summary>
  /// F�brica de conex�es abstratas
  /// </summary>
  TFactoryConnection = class abstract(TInterfacedObject, IDBConnection)
  private
    FStartTransactionAutoConnection: Boolean;
  protected
    FCommandMonitor: ICommandMonitor;
    FDriverConnection: TDriverConnection;
    FDriverTransaction: TDriverTransaction;
  public
    constructor Create(AConnection: TComponent; ADriverName: TDriverName); virtual;
    procedure Connect; virtual; abstract;
    procedure Disconnect; virtual; abstract;
    procedure StartTransaction; virtual;
    procedure Commit; virtual;
    procedure Rollback; virtual;
    procedure ExecuteDirect(const ASQL: string); overload; virtual; abstract;
    procedure ExecuteDirect(const ASQL: string; const AParams: TParams); overload; virtual; abstract;
    procedure ExecuteScript(const ASQL: string); virtual; abstract;
    procedure AddScript(const ASQL: string); virtual; abstract;
    procedure ExecuteScripts; virtual; abstract;
    procedure SetCommandMonitor(AMonitor: ICommandMonitor); virtual;
    function InTransaction: Boolean; virtual; abstract;
    function IsConnected: Boolean; virtual; abstract;
    function GetDriverName: TDriverName; virtual; abstract;
    function CreateQuery: IDBQuery; virtual; abstract;
    function CreateResultSet: IDBResultSet; virtual; abstract;
    function ExecuteSQL(const ASQL: string): IDBResultSet; virtual; abstract;
    function CommandMonitor: ICommandMonitor;
  end;

implementation

{ TFactoryConnection }

function TFactoryConnection.CommandMonitor: ICommandMonitor;
begin
  Result := FCommandMonitor;
end;

procedure TFactoryConnection.Commit;
begin
  if FStartTransactionAutoConnection then
    Disconnect;
end;

constructor TFactoryConnection.Create(AConnection: TComponent; ADriverName: TDriverName);
begin
  FStartTransactionAutoConnection := False;
end;

procedure TFactoryConnection.Rollback;
begin
  if FStartTransactionAutoConnection then
    Disconnect;
end;

procedure TFactoryConnection.SetCommandMonitor(AMonitor: ICommandMonitor);
begin
  FCommandMonitor := AMonitor;
end;

procedure TFactoryConnection.StartTransaction;
begin
  if not IsConnected then
  begin
    Connect;
    FStartTransactionAutoConnection := True;
  End;
end;

end.
