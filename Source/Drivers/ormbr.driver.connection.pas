{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.connection;

interface

uses
  Classes,
  DB,
  Variants,
  ormbr.types.database,
  ormbr.factory.interfaces;

type
  /// <summary>
  /// Classe de conex�es abstract
  /// </summary>
  TDriverConnection = class abstract
  protected
    FDriverName: TDriverName;
  public
    constructor Create(AConnection: TComponent; ADriverName: TDriverName); virtual; abstract;
    procedure Connect; virtual; abstract;
    procedure Disconnect; virtual; abstract;
    procedure ExecuteDirect(const ASQL: string); overload; virtual; abstract;
    procedure ExecuteDirect(const ASQL: string; const AParams: TParams); overload; virtual; abstract;
    procedure ExecuteScript(const ASQL: string); virtual; abstract;
    procedure AddScript(const ASQL: string); virtual; abstract;
    procedure ExecuteScripts; virtual; abstract;
    function IsConnected: Boolean; virtual; abstract;
    function InTransaction: Boolean; virtual; abstract;
    function CreateQuery: IDBQuery; virtual; abstract;
    function CreateResultSet: IDBResultSet; virtual; abstract;
    function ExecuteSQL(const ASQL: string): IDBResultSet; virtual; abstract;
    property DriverName: TDriverName read FDriverName;
  end;

  /// <summary>
  /// Classe de trasa��es abstract
  /// </summary>
  TDriverTransaction = class abstract(TInterfacedObject, IDBTransaction)
  public
    constructor Create(AConnection: TComponent); virtual; abstract;
    procedure StartTransaction; virtual; abstract;
    procedure Commit; virtual; abstract;
    procedure Rollback; virtual; abstract;
    function InTransaction: Boolean; virtual; abstract;
  end;

  TDriverQuery = class(TInterfacedObject, IDBQuery)
  protected
    procedure SetCommandText(ACommandText: string); virtual; abstract;
    function GetCommandText: string; virtual; abstract;
  public
    procedure ExecuteDirect; virtual; abstract;
    function ExecuteQuery: IDBResultSet; virtual; abstract;
    property CommandText: string read GetCommandText write SetCommandText;
  end;

  TDriverResultSetBase  = class(TInterfacedObject)
  end;

  TDriverResultSet<T: TDataSet> = class abstract(TDriverResultSetBase, IDBResultSet)
  private
    FFieldNameInternal: string;
    function GetFetchingAll: Boolean;
    procedure SetFetchingAll(const Value: Boolean);
    function GetDataSet: T;
  protected
    FDataSet: T;
    FRecordCount: Integer;
    FFetchingAll: Boolean;
    FFirstNext: Boolean;
    property DataSet: T read GetDataSet;
  public
    constructor Create(ADataSet: T); virtual;
    destructor Destroy; override;
    procedure Close; virtual;
    function NotEof: Boolean; virtual; abstract;
    function RecordCount: Integer; virtual;
    function FieldDefs: TFieldDefs; virtual;
    function GetFieldValue(AFieldName: string): Variant; overload; virtual; abstract;
    function GetFieldValue(AFieldIndex: Integer): Variant; overload; virtual; abstract;
    function GetFieldType(AFieldName: string): TFieldType; overload; virtual; abstract;
    function FieldByName(AFieldName: string): IDBResultSet; virtual;
    function AsString: string; virtual;
    function AsInteger: Integer; virtual;
    function AsFloat: Double; virtual;
    function AsCurrency: Currency; virtual;
    function AsExtended: Extended; virtual;
    function AsDateTime: TDateTime; virtual;
    function AsVariant: Variant; virtual;
    function AsBoolean: Boolean; virtual;
    function Value: Variant; virtual;
    property FetchingAll: Boolean read GetFetchingAll write SetFetchingAll;
  end;

implementation

uses
  SysUtils;

{ TDriverResultSet<T> }

function TDriverResultSet<T>.AsBoolean: Boolean;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().AsBoolean');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit(False);
  try
    try
      Result := Boolean(LResult);
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

function TDriverResultSet<T>.AsExtended: Extended;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().AsExtended');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit(0);
  try
    try
      Result := Extended(LResult);
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

function TDriverResultSet<T>.AsCurrency: Currency;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().AsCurrency');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit(0);
  try
    try
      Result := Currency(LResult);
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

function TDriverResultSet<T>.AsDateTime: TDateTime;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().AsDateTime');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit(0);
  try
    try
      Result := TDateTime(LResult);
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

function TDriverResultSet<T>.AsFloat: Double;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().AsFloat');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit(0);
  try
    try
      Result := Double(LResult);
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

function TDriverResultSet<T>.AsInteger: Integer;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().AsInteger');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit(0);
  try
    try
      Result := Integer(LResult);
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

function TDriverResultSet<T>.AsString: string;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().AsString');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit('');
  try
    try
      Result := string(LResult);
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

function TDriverResultSet<T>.AsVariant: Variant;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().AsVariant');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit(Null);
  try
    try
      Result := LResult;
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

procedure TDriverResultSet<T>.Close;
begin
  FDataSet.Close;
end;

constructor TDriverResultSet<T>.Create(ADataSet: T);
begin
  /// <summary>
  /// Guarda o RecordCount do �ltimo SELECT executado no IDBResultSet
  /// </summary>
  try
  FRecordCount := FDataSet.RecordCount;
  except
  end;
end;

destructor TDriverResultSet<T>.Destroy;
begin
  inherited;
end;

function TDriverResultSet<T>.FieldByName(AFieldName: string): IDBResultSet;
begin
  FFieldNameInternal := AFieldName;
  Result := Self;
end;

function TDriverResultSet<T>.FieldDefs: TFieldDefs;
begin
  Result := FDataSet.FieldDefs;
end;

function TDriverResultSet<T>.GetDataSet: T;
begin
  Result := FDataSet;
end;

function TDriverResultSet<T>.GetFetchingAll: Boolean;
begin
  Result := FFetchingAll;
end;

function TDriverResultSet<T>.RecordCount: Integer;
begin
  Result := FRecordCount;
end;

procedure TDriverResultSet<T>.SetFetchingAll(const Value: Boolean);
begin
  FFetchingAll := Value;
end;

function TDriverResultSet<T>.Value: Variant;
var
  LResult: Variant;
begin
  if Length(FFieldNameInternal) = 0 then
    raise Exception.Create('Este m�todo s� pode ser usado em sequ�ncia ao m�todo FieldByName().Value');
  LResult := GetFieldValue(FFieldNameInternal);
  if LResult = Null then
    Exit(Null);
  try
    try
      Result := LResult;
    except
      on E: Exception do
      begin
        raise Exception.Create('Error de convers�o de tipo!');
      end;
    end;
  finally
    FFieldNameInternal := '';
  end;
end;

end.
