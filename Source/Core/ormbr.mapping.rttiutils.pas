{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.rttiutils;

interface

uses
  Classes,
  SysUtils,
  Rtti,
  DB,
  TypInfo,
  Math,
  StrUtils,
  Types,
  Variants,
  Generics.Collections,
  /// orm
  ormbr.mapping.attributes,
  ormbr.mapping.classes,
  ormbr.types.mapping;

type
  IRttiSingleton = interface
    ['{AF40524E-2027-46C3-AAAE-5F4267689CD8}']
    function GetRttiType(AClass: TClass): TRttiType;
//    function RunValidade(AClass: TClass): Boolean;
    function MethodCall(AObject: TObject; AMethodName: string; const AParameters: array of TValue): TValue;
  end;

  TRttiSingleton = class(TInterfacedObject, IRttiSingleton)
  private
  class var
    FInstance: IRttiSingleton;
  private
    FContext: TRttiContext;
    constructor CreatePrivate;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    class function GetInstance: IRttiSingleton;
    function GetRttiType(AClass: TClass): TRttiType;
//    function RunValidade(AClass: TClass): Boolean;
    function MethodCall(AObject: TObject; AMethodName: string; const AParameters: array of TValue): TValue;
  end;

implementation

uses
  ormbr.mapping.explorer,
  ormbr.rtti.helper;

{ TRttiSingleton }

constructor TRttiSingleton.Create;
begin
   raise Exception.Create('Para usar o MappingEntity use o m�todo TRttiSingleton.GetInstance()');
end;

constructor TRttiSingleton.CreatePrivate;
begin
   inherited;
   FContext := TRttiContext.Create;
end;

destructor TRttiSingleton.Destroy;
begin
  FContext.Free;
  inherited;
end;

function TRttiSingleton.GetRttiType(AClass: TClass): TRttiType;
begin
  Result := FContext.GetType(AClass);
end;

class function TRttiSingleton.GetInstance: IRttiSingleton;
begin
  if not Assigned(FInstance) then
    FInstance := TRttiSingleton.CreatePrivate;
   Result := FInstance;
end;

//function TRttiSingleton.RunValidade(AClass: TClass): Boolean;
//var
//  LColumn: TColumnMapping;
//  LColumns: TColumnMappingList;
//  LAttribute: TCustomAttribute;
//begin
//  Result := False;
//  LColumns := TMappingExplorer.GetInstance.GetMappingColumn(AClass);
//  for LColumn in LColumns do
//  begin
//     /// <summary>
//     /// Valida se o valor � NULO
//     /// </summary>
//     LAttribute := LColumn.PropertyRtti.GetNotNullConstraint;
//     if LAttribute <> nil then
//       NotNullConstraint(LAttribute).Validate(LColumn.ColumnName, LColumn.PropertyRtti.GetNullableValue(AClass));
//
//     /// <summary>
//     /// Valida se o valor � menor que ZERO
//     /// </summary>
//     LAttribute := LColumn.PropertyRtti.GetZeroConstraint;
//     if LAttribute <> nil then
//        ZeroConstraint(LAttribute).Validate(LColumn.ColumnName, LColumn.PropertyRtti.GetNullableValue(AClass));
//  end;
//  Result := True;
//end;

function TRttiSingleton.MethodCall(AObject: TObject; AMethodName: string;
  const AParameters: array of TValue): TValue;
var
  LRttiType: TRttiType;
  LMethod: TRttiMethod;
begin
  LRttiType := GetRttiType(AObject.ClassType);
  LMethod   := LRttiType.GetMethod(AMethodName);
  if Assigned(LMethod) then
     Result := LMethod.Invoke(AObject, AParameters)
  else
     raise Exception.CreateFmt('Cannot find method "%s" in the object',[AMethodName]);
end;

end.

